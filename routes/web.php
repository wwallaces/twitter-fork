<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@index');
// Route::get('/{user}', 'UserController@show');


//SESSION//
Route::get('home', 'SessionController@show')->name('home')->middleware('auth');
Route::get('login', 'SessionController@create')->name('login')->middleware('guest');
Route::post('login', 'SessionController@store');
Route::get('logout', 'SessionController@destroy')->name('logout');
//SESSION//

//REGISTRAION//
Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');
//END OF REGISTRATION//

//PROFILE//
Route::get('/profile/{user}', 'UserController@show');
Route::get('/profile/{user}', 'UserController@profile');
Route::get('/profile/{user}/edit', 'UserController@edit');
Route::post('/profile/{user}', 'UserController@update_avatar');
Route::put('/profile/{user}', 'UserController@update_profile');
Route::delete('/profile/{user}/delete', 'UserController@destroy');
//END OF PROFILE//

//FOLLOWERS//
Route::get('/profile/{user}/follow', 'UserController@followUser')->name('user.follow');
Route::get('/profile/{user}/unfollow', 'UserController@unFollowUser')->name('user.unfollow');
//END OF FOLLOWERS//
// TWEETS//
Route::get('post/{post}/like', 'PostController@likepost');
Route::get('post/{post}/dislike','PostController@dislikepost');
Route::get('post', 'UserController@getfeed');
//END OF TWEETS//
//CRUD//
Route::get('post/{post}/like', 'UserController@likepost')->name('show.like');
Route::get('post/{post}/dislike','UserController@dislikepost')->name('show.dislike');
Route::resource('post', 'PostController');
Route::get('post', 'PostController@getFeed');
Route::post('/post/{post}/comments', 'CommentsController@store');
Route::get('/post/{post}/comments/edit', 'CommentsController@edit');
Route::put('/post/{post}/comments', 'CommentsController@update');
Route::delete('/post/{post}/delete', 'CommentsController@destroy');
//END OF CRUD//
