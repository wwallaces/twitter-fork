@extends('layouts.master')

@section('content')



    <div class="fixed">

        <div class="container bg">
            <label> Followers:</label>
        <p>{{ $totalFollowers }}</p>

            <label>Followings:</label>
            <p>{{ $totalFollowings }}</p>

            <label>Posts</label>
            <p>{{ $totalPosts }}</p>
        <ul>
            <label>Followers:</label>
            @foreach($followers as $follows)
            <li>
                <p>{{ $follows->name }}</p>
            </li>
        @endforeach
        </ul>
        <ul>
            <label>Followings:</label>
            @foreach($followings as $follows)
            <li>
                <p>{{ $follows->name }}</p>
            </li>
        @endforeach
        </ul>
        </div>
    </div>

@endsection
