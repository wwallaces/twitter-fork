<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/profile.css">
    <link rel="stylesheet" href="/css/fonts/font.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <title>Edit Profile</title>
</head>

<body>
    <div class="grid">
        <div class="head">
            <div class="logo">
                <img src="/css/img/stix-logo.png" alt="logo">
            </div>
            <div class="nav">
                <h2><a href="/">Home<span class="faicon"><i class="fa fa-home"></span></i></a></h2>
                <h2><a href="/profile/{{ Auth::user()->id }}">Profile<span class="faicon"><i class="fa fa-user"></span></i></a></h2>
                <h2><a href="/logout">Logout<span class="faicon"><i class="fa fa-power-off"></span></i></a></h2>
            </div>
        </div>
        <div class="aside">
            <div class="aside">
                <div class="avatar-card">
                    <div class="profile-avatar">
                        <img src="/uploads/avatars/{{  Auth::user()->avatar }}" alt="avatar">
                    </div>
                    <div class="profile-name">
                        <h2>{{Auth::user()->name}}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="main">
            <div class="edit-profile">

                <form enctype="multipart/form-data" action="/profile/{{ Auth::user()->id }}" method="POST">
                    <input type="hidden" name="_method" value="PUT" />
                    <!-- {{ csrf_field () }} -->

                    <div class="profile-item">
                    <label>Update Profile Image:</label>
                    <input type="file" name="avatar">
                    </div>
                    <div class="profile-item">
                        <label>Biography(limit:187):</label>
                        <textarea name="bio" type="text" placeholder="Write about yourself" maxlength="187">{{ $user->profile->bio }}</textarea>
                    </div>
                    <div class="profile-item">
                        <label>Birthplace:</label>
                        <input type="text" name="birthplace" placeholder="Where is the Motherland?" value="{{ $user->profile->birthplace }}">
                    </div>
                    <div class="profile-item">
                    <h4>Birthday:</h4>
                        <div class="row">
                            <label>Month:</label>
                            <select id="inputMonth" name="birth_month">
                            <option>January</option>
                            <option>February</option>
                            <option>March</option>
                            <option>April</option>
                            <option>May</option>
                            <option>June</option>
                            <option>July</option>
                            <option>August</option>
                            <option>September</option>
                            <option>October</option>
                            <option>November</option>
                            <option>December</option>
                        </select>
                        </div>
                        <div class="row">
                            <label>Day:</label>
                            <select name="birth_day" id="input_Birthday">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                                <option>13</option>
                                <option>14</option>
                                <option>15</option>
                                <option>16</option>
                                <option>17</option>
                                <option>18</option>
                                <option>19</option>
                                <option>20</option>
                                <option>21</option>
                                <option>22</option>
                                <option>23</option>
                                <option>24</option>
                                <option>25</option>
                                <option>26</option>
                                <option>27</option>
                                <option>28</option>
                                <option>29</option>
                                <option>30</option>
                                <option>31</option>
                            </select>
                        </div>
                        <div class="row">
                            <label>Year:</label>
                            <select name="birth_year" id="input_Year">
                            <option>2000</option>
                            <option>1999</option>
                            <option>1998</option>
                            <option>1997</option>
                            <option>1996</option>
                            <option>1995</option>
                            <option>1994</option>
                            <option>1993</option>
                            <option>1992</option>
                            <option>1991</option>
                            <option>1990</option>
                            <option>1989</option>
                            <option>1988</option>
                            <option>1987</option>
                            <option>1986</option>
                            <option>1985</option>
                            <option>1984</option>
                            <option>1983</option>
                            <option>1982</option>
                            <option>1981</option>
                            <option>1980</option>
                            <option>1979</option>
                        </select>
                        </div>
                    </div>
                    <div class="profile-item">
                        <label>
                            Location:
                        </label>
                        <input type="text" name="location" value="{{ $user->profile->location }}" placeholder="Enter your Location">
                    </div>
                    <div class="profile-item">
                        <label>
                            Phone Number:
                        </label>
                        <input type="text" name="phone_number" value="{{ $user->profile->phone_number }}" placeholder="Enter your Phone Number">
                    </div>
                    <div class="profile-item">
                        <label>
                            Education:
                        </label>
                        <input type="text" name="education" value="{{ $user->profile->education }}" placeholder="Where did you study?">
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token( )}}">
                    <button type="submit">Submit</button>
                </form>
                <form action="/profile/{{ Auth::user()->id }}/delete" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <!-- {{ csrf_field() }} -->

                    <button type="submit" class="delete">Delete Account</button>
                </form>
            </div>


            </div>

            <div class="footer">
                <p>
                    Copyright &copy; JJM 2018
                </p>
            </div>
        </div>
</body>

</html>
