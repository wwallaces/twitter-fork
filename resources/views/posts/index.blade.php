@extends('layouts.master')

@section('content')
<div class="main">
    <div class="textarea">
        <form action="/post" method="post">
            {{ csrf_field() }}
            <textarea name="body" placeholder="Write a post" required></textarea>
            <button type="submit"> Stix</button>
        </form>
    </div>
    <div class="feed">
        <ul class="timeline">
            @foreach($posts as $post)
            <li class="tweet">
                <div class="tweet-avatar">
                    <div class="tweetlogo">
                        <a href="/profile/{{ $post->user_id }}"><img src="/uploads/avatars/{{  $post->user->avatar }}" alt="avatar"></a>
                    </div>
                </div>
                <div class="tweet-name">
                    <h2>{{ $post->user->name }}</h2>
                </div>
                <div class="tweet-body">
                    <div class="tweet-bubble">
                        <a href="/post/{{ $post->id }}">
                            <p>
                                {{ $post->body }}
                            </p>
                        </a>
                    </div>
                </div>
                <div class="tweet-interaction">
                    <a href="" class="like"><i class="fa fa-thumbs-up"></i></a>
                    <a href="" class="like"><i class="fa fa-thumbs-down"></i></a>
                </div>
                <div class="tweet-datetime">
                    <em>
                        {{ $post->created_at->diffForHumans() }}
                    </em>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
@endsection
