<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>

    <meta charset="utf-8">

    <link rel="stylesheet" href="/css/master.css">

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <title>Stix - Homepage</title>

</head>
<body>

        <div class="grid">
            <div class="head">
                @include('layouts.head')
            </div>

            <div class="aside">
                @include('layouts.aside')
            </div>

            @yield('content')


            <div class="footer">
                <p>
                    Copyright &copy; JJM 2018
                </p>
            </div>
        </div>
</body>
</html>
