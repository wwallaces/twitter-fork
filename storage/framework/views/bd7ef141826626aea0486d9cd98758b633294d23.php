<?php $__env->startSection('content'); ?>

<div class="main" id="show">
    <div class="editPost">
        <h2> Edit A Post </h2>
    <div class="showPost">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam ea, autem, odio pariatur minima ratione voluptatum hic repudiandae doloribus consectetur.</p>
    </div>
    <div class="editPostForm">
        <form action="/post/<?php echo e($comments->id); ?>" method="POST">
            <input type="hidden" name="_method" value="PUT" />
            <?php echo e(csrf_field()); ?>

            <div class="form-group">
                <textarea class="form-control" name="body" placeholder="Write a post"><?php echo e($comments->body); ?></textarea>
            </div>
            <div class="form-group">
                <button type="submit">Update Stix</button>
            </form>
            <form action="/post/<?php echo e($comments->id); ?>" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                <?php echo e(csrf_field()); ?>

                <button type="submit" class="deletebtn">Delete Post</button>
            </form>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>