<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/register.css">
    <link rel="stylesheet" href="/css/fonts/font.css">
    <title>Register</title>
</head>

<body>

    <div class="grid">
        <div class="main">
            <div class="content">
                <div class="title">
                    <h1>Stix</h1>
                </div>
                <div class="message">
                    <p>
                        Connect with your friends and other fascinating people.
                        Get in-the-moment updates on the things that interest you.
                    </p>
                </div>
                <div class="copyright">
                    <p>Copyright &copy; JJM 2018</p>
                </div>
            </div>
        </div>

        <div class="register">
            <form method="POST" action="/register">
                <?php echo e(csrf_field()); ?>

                <div class="title">
                    <h2>Be a Member</h2>
                </div>
                <div class="form-group">
                    <input type="text" id="name" name="name" placeholder="Enter your Name" required>
                </div>
                <div class="gender">
                    <h4>Gender:</h4>
                    <select name="gender" id="inputGender">
                        <option>Male</option>
                        <option>Female</option>
                        <option>???</option>
                    </select>
                </div>
                <div class="birthday">
                    <h4>Birthday:</h4>
                    <select id="inputMonth" name="birth_month">
                        <option>January</option>
                        <option>February</option>
                        <option>March</option>
                        <option>April</option>
                        <option>May</option>
                        <option>June</option>
                        <option>July</option>
                        <option>August</option>
                        <option>September</option>
                        <option>October</option>
                        <option>November</option>
                        <option>December</option>
                    </select>
                    <select id="inputBirthDay" name="birth_day">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                        <option>11</option>
                        <option>12</option>
                        <option>13</option>
                        <option>14</option>
                        <option>15</option>
                        <option>16</option>
                        <option>17</option>
                        <option>18</option>
                        <option>19</option>
                        <option>20</option>
                        <option>21</option>
                        <option>22</option>
                        <option>23</option>
                        <option>24</option>
                        <option>25</option>
                        <option>26</option>
                        <option>27</option>
                        <option>28</option>
                        <option>29</option>
                        <option>30</option>
                        <option>31</option>
                    </select>
                    <select name="birth_year" id="inputYear">
                        <option>2000</option>
                        <option>1999</option>
                        <option>1998</option>
                        <option>1997</option>
                        <option>1996</option>
                        <option>1995</option>
                        <option>1994</option>
                        <option>1993</option>
                        <option>1992</option>
                        <option>1991</option>
                        <option>1990</option>
                        <option>1989</option>
                        <option>1988</option>
                        <option>1987</option>
                        <option>1986</option>
                        <option>1985</option>
                        <option>1984</option>
                        <option>1983</option>
                        <option>1982</option>
                        <option>1981</option>
                        <option>1980</option>
                        <option>1979</option>
                    </select>

                </div>


                <div class="form-group">
                    <input type="text" class="form-control" name="birthplace" id="inputBirthPlace" placeholder="Enter your Birthplace">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter your Email" required>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter your Password" required>
                </div>
                <div class="form-group">
                    <button type="submit">Register</button>
                </div>
            </form>
        </div>
    </div>

</body>

</html>
