<?php $__env->startSection('content'); ?>



    <div class="fixed">

        <div class="container bg">
            <label> Followers:</label>
        <p><?php echo e($totalFollowers); ?></p>

            <label>Followings:</label>
            <p><?php echo e($totalFollowings); ?></p>

            <label>Posts</label>
            <p><?php echo e($totalPosts); ?></p>
        <ul>
            <label>Followers:</label>
            <?php $__currentLoopData = $followers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $follows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li>
                <p><?php echo e($follows->name); ?></p>
            </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
        <ul>
            <label>Followings:</label>
            <?php $__currentLoopData = $followings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $follows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li>
                <p><?php echo e($follows->name); ?></p>
            </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>