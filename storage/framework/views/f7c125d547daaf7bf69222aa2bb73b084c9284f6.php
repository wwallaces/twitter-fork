<?php $__env->startSection('content'); ?>




<div class="container fixed">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <img src="/uploads/avatars/<?php echo e($user->avatar); ?>" class="user-avatar">
            <h2><?php echo e($user->name); ?>'s Profile</h2>
            <p><strong>From: </strong><?php echo e($user->profile->birthplace); ?></p>
            <p><strong>Birthday: </strong><?php echo e($user->profile->birth_month); ?>, <?php echo e($user->profile->birth_year); ?></p>
            <?php if(Auth::user()): ?>
            <a href="/profile/<?php echo e(Auth::user()->id); ?>/edit">Edit Profile</a>
            <?php else: ?>

            <?php endif; ?>
        </div>
        <div class="col-md-10 ">
            <p><?php echo e($user->profile->bio); ?></p>
        </div>
    </div>
</div>
<hr>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWc+VRZkx5xU3daN+lYb49+Ax+Tl" crossorigin="anonymous"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>