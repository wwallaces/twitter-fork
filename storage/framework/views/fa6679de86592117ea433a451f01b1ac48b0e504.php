<div class="card">
    <div class="card-head">
        <div class="card-avatar">
            <img src="/uploads/avatars/<?php echo e(Auth::user()->avatar); ?>" alt="avatar">
        </div>
        <h2><?php echo e(Auth::user()->name); ?></h2>
    </div>
    <div class="card-body">
        <ul class="card-profile">
            <li>
                <h4>Birthday: </h4>
                <p>
                    <?php echo e(Auth::user()->profile->birth_month); ?> <?php echo e(Auth::user()->profile->birth_day); ?>,<?php echo e(Auth::user()->profile->birth_year); ?>

                </p>
            </li>
            <li>
                <h4>Gender: </h4>
                <p>
                    Male
                </p>
            </li>
            <li>
                <h4>Bio: </h4>
                <p>
                    <?php echo e(Auth::user()->profile->bio); ?>

                </p>
            </li>
        </ul>
    </div>
    <div class="card-bottom">
            <div class="card-counter-item">
                <h4>Post:</h4>
                <p>10</p>
            </div>
            <div class="card-counter-item">
                <h4>Followers:</h4>
                <p>25</p>
            </div>
            <div class="card-counter-item">
                <h4>Followings:</h4>
                <p>15</p>
            </div>
    </div>
    <div class="card-footer">
        <a href="/"><i class="fa fa-home"></i></a>
        <a href="/profile/<?php echo e(Auth::user()->id); ?>"><i class="fa fa-user"></i></a>
        <a href="/logout"><i class="fa fa-power-off"></i></a>
    </div>
</div>
