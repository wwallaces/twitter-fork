<?php $__env->startSection('content'); ?>

<div class="main">

</div>

<div class="main" id="show">
    <div class="tweet-show">
        <div class="tweet-avatar">
            <div class="tweetlogo">
                <img src="/uploads/avatars/<?php echo e($posts->user->avatar); ?>" alt="avatar">
            </div>
        </div>
        <div class="tweet-name">
            <h2><?php echo e($posts->user->name); ?></h2>
        </div>
        <div class="tweet-body">
            <div class="tweet-bubble">
                <p>
                    <?php echo e($posts->body); ?>

                </p>
            </div>
        </div>
        <div class="tweet-interaction">
            <a href="/post/{post}/like" class="like"><i class="fa fa-thumbs-up"></i></a>
            <a href="/post/{post}/dislike" class="like"><i class="fa fa-thumbs-down"></i></a>
            <?php if(Auth::id() == $posts->user_id): ?>
            <a href="/post/<?php echo e($posts->id); ?>/edit">Edit</a>
            <?php endif; ?>
        </div>
        <div class="tweet-datetime">
            <em>
                <?php echo e($posts->created_at->diffForHumans()); ?>

            </em>
        </div>
    </div>

    <div class="comments">
        <ul class="commentline">
            <?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li>
                <div class="comment-avatar">
                    <a href="/profile/<?php echo e($comment->user_id); ?>"><img src="/uploads/avatars/<?php echo e($comment->user->avatar); ?>" ></a>
                </div>
                <div class="commentBody">
                    <div class="commentBubble">
                        <h3><?php echo e($comment->user->name); ?></h3>
                        <br/>
                        <p><?php echo e($comment->body); ?></p>
                    </div>
                    <div class="datetime">
                        <em> <?php echo e($comment->created_at->diffForHumans()); ?> </em>
                        <?php if(Auth::id() == $comment->user_id): ?>
                        <a href="/post/<?php echo e($comment->id); ?>/comments/edit">Edit</a>
                        <?php endif; ?>
                    </div>
                </div>
            </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
        <div class="commentForm">
            <form method="POST" action="/post/<?php echo e($posts->id); ?>/comments">
                <?php echo e(csrf_field()); ?>

                <div class="form-group">
                    <textarea name="body" placeholder="Your Comment here." class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Comment</button>
                </div>
            </form>
        </div>
    </div>


</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>