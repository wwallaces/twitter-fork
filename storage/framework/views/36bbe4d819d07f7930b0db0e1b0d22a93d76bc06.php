<?php $__env->startSection('content'); ?>


<div class="container fixed">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <img src="/uploads/avatars/<?php echo e($user->avatar); ?>" class="user-avatar">
            <h2>Edit <?php echo e($user->name); ?>'s Profile</h2>
            <form enctype="multipart/form-data" action="/profile/<?php echo e(Auth::user()->id); ?>" method="POST">
                <input type="hidden" name="_method" value="PUT" />
                <?php echo e(csrf_field ()); ?>


                <label>Update Profile Image</label>
                <input type="file" name="avatar">
                <hr>
                <label>Biography(limit:100)</label>
                <textarea name="bio" type="text" class="form-control" placeholder="Write about yourself" maxlength="187"><?php echo e($user->profile->bio); ?></textarea>
                <hr>
                <label>Birthplace</label>
                <input type="text" name="birthplace" class="form-control" placeholder="Where is the Motherland?" value="<?php echo e($user->profile->birthplace); ?>">
                <hr>
                <label>Birthday</label>
                <div class="row">
                    <div class="col">
                        <label>Month</label>
                        <select class="form-control" id="inputMonth" name="birth_month">
                            <option>January</option>
                            <option>February</option>
                            <option>March</option>
                            <option>April</option>
                            <option>May</option>
                            <option>June</option>
                            <option>July</option>
                            <option>August</option>
                            <option>September</option>
                            <option>October</option>
                            <option>November</option>
                            <option>December</option>
                        </select>
                    </div>
                    <div class="col">
                        <label>Year</label>
                        <select class="form-control" name="birth_year" id="inputYear">
                            <option>2000</option>
                            <option>1999</option>
                            <option>1998</option>
                            <option>1997</option>
                            <option>1996</option>
                            <option>1995</option>
                            <option>1994</option>
                            <option>1993</option>
                            <option>1992</option>
                            <option>1991</option>
                            <option>1990</option>
                            <option>1989</option>
                            <option>1988</option>
                            <option>1987</option>
                            <option>1986</option>
                            <option>1985</option>
                            <option>1984</option>
                            <option>1983</option>
                            <option>1982</option>
                            <option>1981</option>
                            <option>1980</option>
                            <option>1979</option>
                        </select>
                    </div>
                </div>

                <hr>

               <input type="hidden" name="_token" value="<?php echo e(csrf_token( )); ?>">
               <button type="submit" class="pull-right btn btn-sm btn-primary">Submit</button>
            </form>
            <form action="/profile/<?php echo e(Auth::user()->id); ?>/delete" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                <?php echo e(csrf_field()); ?>

                <button type="submit" class="btn btn-danger btn-sm float-right">Delete Account</button>
            </form>
        </div>
    </div>
</div>







<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>