<?php $__env->startSection('content'); ?>
<div class="main">
    <div class="textarea">
        <form action="/post" method="post">
            <?php echo e(csrf_field()); ?>

            <textarea name="body" placeholder="Write a post" required></textarea>
            <button type="submit"> Stix</button>
        </form>
    </div>
    <div class="feed">
        <ul class="timeline">
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="tweet">
                <div class="tweet-avatar">
                    <div class="tweetlogo">
                        <a href="/profile/<?php echo e($post->user_id); ?>"><img src="/uploads/avatars/<?php echo e($post->user->avatar); ?>" alt="avatar"></a>
                    </div>
                </div>
                <div class="tweet-name">
                    <h2><?php echo e($post->user->name); ?></h2>
                </div>
                <div class="tweet-body">
                    <div class="tweet-bubble">
                        <a href="/post/<?php echo e($post->id); ?>">
                            <p>
                                <?php echo e($post->body); ?>

                            </p>
                        </a>
                    </div>
                </div>
                <div class="tweet-interaction">
                    <a href="" class="like"><i class="fa fa-thumbs-up"></i></a>
                    <a href="" class="like"><i class="fa fa-thumbs-down"></i></a>
                </div>
                <div class="tweet-datetime">
                    <em>
                        <?php echo e($post->created_at->diffForHumans()); ?>

                    </em>
                </div>
            </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>