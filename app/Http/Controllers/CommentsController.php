<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Comment;
use Auth;
class CommentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Post $post, Request $request)
    {
        // $data['body'] = request('body');
        // $data['post_id'] = request('post_id');
        //
        // $comment = Comment::create($data);
        //

        // Comment::create([
        //     'body' => request('body'),
        //     'post_id' => $post->id
        $data['body'] = request('body');
        $data['user_id'] = auth()->id();


        $post->addComment($data);

        // auth()->user()->publish(
        //     new Comment(request(['body']))
        // );
        return redirect('post/' .$post->id);
    }

    public function edit($id)
    {
        $comments = Comment::find($id);
        if(Auth::id() == $comments->user_id);
        return view('comments.edit', compact('comments'));
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $comments = Comment::find($id);

        $comments->body = $request->input('body');

        $comments->save();
         return redirect('/post/' .$comments->id);
    }

    public function destroy($id)
    {

        $post = Post::find($id);

        Comment::destroy($id);

        return redirect('/post/' .$post->id);


    }
}
