<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class SessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('destroy');
    }

    public function create()
    {
        return view ('auth.login');
    }

    public function show(User $user, $id)
    {
        $user = Auth::user();
        return view('layouts.master');
    }

    public function store(Request $request)
    {
        if(! auth()->attempt(request(['email','password']))){
            session()->flash('errorMessage', 'Invalid credentials');
            return redirect()->back();
        }


        session()->flash('message', 'You have successfully logged in');
        return redirect('/');
    }


    public function destroy()
    {
        auth()->logout();

        return redirect('/login');
    }
}
