<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use Auth;
use App\User;
use App\Profile;
use DB;
use App\Quotation;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    public function index()
    {

        // $user = User::find(auth()->user()->id);
        // $followings = $user->followings;
        // $post = Post::find($followings);
        // $feeds = count($post);
        //
        // $followees = DB::table('users')
        //     ->join('posts', 'user_id', '=', 'posts.user_id')
        //     ->join('followers', 'follower_id', '=', 'followers.follower_id')
        //     ->select('users.*', 'posts.user_id', 'followers.user_id')
        //     ->get();

            $posts = Post::join('followers', 'posts.user_id', '=', 'followers.follower_id')
            ->where('posts.user_id',auth()->user()->id)->get();

                // ->join('followers', 'follower_id', '=', 'followers.follower_id')
                // ->select('users.*', 'posts.user_id', 'followers.user_id')
                // ->get();

        //$posts = Post::class;


        // return view('posts.index', compact('followees','posts'));
        // // // return view ()
        //
            $posts = Post::all();
            return view('posts.index', compact('posts'));
        }

        public function showfeed($id)
        {
            $postId = Post::find($id);
            $user = User::find($postId);
            $followers = $user->followers;
            $followings = $user->followings;
            $posts = $user->posts;


            $totalFollowers = count($followers);
            $totalFollowings = count($followings);
            $totalPosts = count($posts);

                return view ('user.show', compact('user', 'followers', 'followings', 'totalFollowers', 'totalFollowings','totalPosts'));
        }


    public function show($id)
    {

        $posts = Post::find($id);
        $comments = $posts->comments;
        return view ('posts.show', compact('posts', 'comments', 'likes', 'dislikes', 'totalLikes', 'totalDislikes'));
    }

    public function likepost($id)
    {
        // $post = DB::table('likes')->select('post_id')->select('user_id')->get();
        $post = Post::find($id);
        if(! $post) {
            return redirect()->back()->with('error', 'Post does not exist.');
        }

        $duplicate = DB::table('likes')
                ->where([
                    ['user_id', '=', auth()->id()],
                    ['post_id', '=', $id]
                ])
                ->count();

        if($duplicate){

        }
        DB::insert('dislikes')
        ->where([
            ['user_id', '=', auth()->id()],
            ['post_id', '=', $id]
        ])
        ->count();
        $post->dislikes()->detach(auth()->user()->id);
        $post->likes()->attach(auth()->user()->id);

        return redirect()->back()->with('success', 'Succesfully liked the post.');

    }

    public function dislikepost($id)
    {
        // $post = DB::table('likes')->select('post_id')->select('user_id')->get();
        $post = Post::find($id);
        if(! $post) {
            return redirect()->back()->with('error', 'Post does not exist.');
        }

        $duplicate = DB::table('dislikes')
                ->where([
                    ['user_id', '=', auth()->id()],
                    ['post_id', '=', $id]
                ])
                ->count();

        if($duplicate){

        }
        DB::insert('likes')
        ->where([
            ['user_id', '=', auth()->id()],
            ['post_id', '=', $id]
        ])
        ->count();

        $post->dislikes()->attach(auth()->user()->id);

        return redirect()->back()->with('success', 'Succesfully disliked the post.');
    }


    public function store(Request $request)
    {
        $data['body'] = request('body');
        $data['user_id'] = auth()->id();
        // $data['user_id'] = auth()->id();

        auth()->user()->publish(
            new Post(request(['body']))
        );


        return redirect('/');
    }

    public function edit($id)
    {
        $posts = Post::find($id);
        if(Auth::id() == $posts->user_id);{

            return view('posts.edit', compact('posts'));
        }
    }

    public function update(Request $request, $id)
    {
        $posts = Post::find($id);

        $posts->body = request('body');

        $posts->save();
        return redirect('/post/' .$posts->id);
    }

    public function destroy($id)
    {
        Post::destroy($id);

        return redirect ('/');
    }

    public function profilePost()
    {
        $id = Auth::user()->id;
        $followees =
        DB::table('followers')->select('follow_id')->where('user_id', $id);


    }


}
