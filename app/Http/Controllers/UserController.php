<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use App\Profile;
use App\User;
use Illuminate\Notifications\Notification;
use App\Post;
use DB;

class UserController extends Controller
{

    public function __construct(){
        $this->middleware('auth')->except('profile');
    }

    public function profile($id = null, Profile $profile)
    {
        if(is_null($id) ){
            $user = User::find(Auth::user()->id);
        }else{

            $user = User::find($id);
        }

        return view ('user.userprofile', compact('profile', 'user'));

    }



    public function edit(Profile $profile)
    {
        return view ('user.editprofile', array('user' => Auth::user()));
        $profile = Profile::find($id);
        if(Auth::id() == $profile->user_id);{

            return view('posts.edit', compact('profile'));
        }
    }

    public function update_profile($id, Request $request)
    {
        // Handle the user upload of avatar
        if($request->hasFile('avatar'))
        {
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)-> save(public_path('/uploads/avatars/' . $filename));

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }



        $user = auth()->user();
        $profile = Profile::where('user_id', $id)->first();

        $profile->bio = request('bio');
        $profile->gender  = request('gender');
        $profile->location = request('location');
        $profile->education = request('education');
        $profile->phone_number = request('phone_number');
        $profile->birthplace = request('birthplace');
        $profile->birth_day = request('birth_day');
        $profile->birth_month = request('birth_month');
        $profile->birth_year = request('birth_year');

        $profile->save();

        return view ('user.userprofile', compact(['user', 'profile']));
    }


    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/login');
    }

    public function followUser(int $profileId)
    {
        $user = User::find($profileId);
        if(! $user) {
            return redirect()->back()->with('error', 'User does not exist.');
        }

        $user->followers()->attach(auth()->user()->id);

        return redirect()->back()->with('success', 'Succesfully followed the user.');
    }

    public function unFollowUser(int $profileId)
    {
        $user = User::find($profileId);
        if(! $user) {
            return redirect()->back()->with('error', 'User does not exist.');
        }

        $user->followers()->detach(auth()->user()->id);
        return redirect()->back()->with('success', 'Successfully unfollowed the user.');
    }

    public function likepost($id)
    {
        // $post = DB::table('likes')->select('post_id')->select('user_id')->get();
        $post = Post::find($id);
        if(! $post) {
            return redirect()->back()->with('error', 'Post does not exist.');
        }

        $post->likes()->attach(auth()->user()->id);

        return redirect()->back()->with('success', 'Succesfully liked the post.');

    }

    public function dislikepost($id)
    {
        // $post = DB::table('likes')->select('post_id')->select('user_id')->get();
        $post = Post::find($id);
        if(! $post) {
            return redirect()->back()->with('error', 'Post does not exist.');
        }

        $post->dislikes()->detach(auth()->user()->id);

        return redirect()->back()->with('success', 'Succesfully disliked the post.');
    }


    public function show(int $userId)
    {
        $user = User::find($userId);
        $followers = $user->followers;
        $followings = $user->followings;
        $posts = $user->posts;


        $totalFollowers = count($followers);
        $totalFollowings = count($followings);
        $totalPosts = count($posts);

            return view ('user.show', compact('user', 'followers', 'followings', 'totalFollowers', 'totalFollowings','totalPosts'));
    }
    public function likesCounter($id)
    {
        $post = Post::find($id);
        $likes = $post->likes;
        $dislikes = $post->dislikes;

        $totalLikes = count($likes);
        $totalDislikes = count($dislikes);

        return view ('posts.index', compact('post', 'likes', 'dislikes', 'totalLikes', 'totalDislikes'));
    }

}
