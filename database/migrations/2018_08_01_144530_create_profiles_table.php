<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->integer('user_id');
            $table->text('bio')->default(null);
            $table->string('gender')->default(null);
            $table->string('birthplace')->default(null);
            $table->string('birth_month', 10)->default(null);
            $table->integer('birth_day')->default(null);
            $table->year('birth_year')->default(null);
            $table->string('phone_number')->default(null);
            $table->string('location')->default(null);
            $table->string('education')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
